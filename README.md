# Server Side Chat

We want you to implement a simple text based chat based on Netty’s framework latest version.
Chat is a channel based communication tool, so our fancy chat implementation should support
multiple channels for users communications. There is one restriction though: a user can only
join one channel at a time, when they join another they leave their current channel. Moreover,
the same user can auth twice from different devices, and on both them they should be able to
receive messages.

ChatServer should handle the following commands:

- `/login <name> <password>`
  If the user doesn’t exists, create profile else login, after login join to last connected
  channel (use join logic, if client’s limit exceeded, keep connected, but without active
  channel).
- `/join <channel>`
  Try to join a channel (max 10 active clients per channel is needed). If client's limit
  exceeded - send error, otherwise join channel and send last N messages of activity.
- `/leave`
  Leave current channel.
- `/disconnect`
  Close connection to server.
- `/list`
  Send list of channels.
- `/users`
  Send list of unique users in current channel.
- `<text message terminated with CR>`
  Sends message to current channel Server must send a new message to all connected to
  this channel.

## Build and Usage

### Build

build

```shell
./mvnw clean compile assembly:single

```

run (with java 15)

```shell
java -jar target/ChatServer-1.0-SNAPSHOT-jar-with-dependencies.jar 
```

### Usage

Open shell, connect to server

```shell
telnet localhost 8023
```

login using any username pass

```shell
/login myusername mypass
```

display all channels

```shell
/list
```

join desired channel

```shell
/join alpha
```

show active users in channel

```shell
/users
```

say "hello"

```shell
hello everyone!
```

leave once you finished

```shell
/leave
```

disconnect when ready

```shell
/disconnect
```

## Roadmap

1. password could be secured, for example, via `bcrypt`
2. logging could be enhanced (do not use `System.out.println()`)
3. telnet could be secured with SSL
4. DI could awesome fit here (i.g. Spring Boot)
5. Not sure about good practices used

## Conclusion

Overall I think it good solid start of that type of project. It does its job, as Proof of Concept it works well.

All requirements satisfied(10 messages on join, multiple sessions, auto-join, participants limit)

About practices: I'm using `ctx.pipeline().get(Handler.class).channelRead0(ctx, msg)`. What I found out I could do
something like
`ctx.fireEventRead(IncomingMsg)` and check type within handler:

```java
if(!(msg instanceof MyHandlerMsg)){
        ctx.fireEventRead(msg); //pass it further
        return;
        }
        var myHandlerMsg=(MyHandlerMsg)msg;
//do whatever I want with msg
```

Uncertain solution: I'm confused with bottleneck in `ChatHandler` as requirement.

Another possible approach is to move message creation to `MessageFactory`(LoginMessage/LeaveMessage/etc) and
get `IncomingMessage` as result, and use mechanic described above.

Another possible approach is get rid of `SimpleChannelInboundHandler` extension in these handlers and use them as simple
use cases.

### Concurrency

Couple of ways were used to provide thread safe:

- using `concurrent hash map` to provide sync access to data
- storing attributes in `ChannelHandlerContext` instead of storing data in handlers

### Patterns used

- factory
- singleton
- adapter
- state/command inspired for handlers
- 
