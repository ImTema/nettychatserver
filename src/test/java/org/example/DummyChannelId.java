package org.example;

import io.netty.channel.ChannelId;
import lombok.AllArgsConstructor;

import java.util.Objects;

@AllArgsConstructor
public class DummyChannelId implements ChannelId {

    private final String name;

    @Override
    public String asShortText() {
        return toString();
    }

    @Override
    public String asLongText() {
        return toString();
    }

    @Override
    public int compareTo(final ChannelId o) {
        if (o instanceof DummyChannelId) {
            return 0;
        }

        return asLongText().compareTo(o.asLongText());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DummyChannelId that = (DummyChannelId) o;

        if (!Objects.equals(name, that.name)) return false;
        return Objects.equals(name, that.name);
    }

    @Override
    public String toString() {
        return name;
    }
}
