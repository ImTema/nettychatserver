package org.example;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.embedded.EmbeddedChannel;
import org.example.handlers.ChatHandler;
import org.example.handlers.JoinHandler;
import org.example.handlers.LeaveHandler;
import org.example.handlers.LoginHandler;
import org.example.models.ResponseMessage;
import org.example.services.RequestDecoder;

import java.nio.charset.Charset;

public class Quick {

    public static ByteBuf toByteBuf(String prompt) {
        return ByteBufUtil.writeUtf8(ByteBufAllocator.DEFAULT, prompt);
    }

    public static String toText(EmbeddedChannel channel) {
        Object obj = channel.readOutbound();
        if (obj instanceof ByteBuf) {
            return ((ByteBuf) obj).toString(Charset.defaultCharset());
        } else if (obj instanceof ResponseMessage) {
            return ((ResponseMessage) obj).getMessage();
        }
        return null;
    }

    public static void log(EmbeddedChannel channel) {
        var text = Quick.toText(channel);
        while (text != null) {
            System.out.println(text);
            text = Quick.toText(channel);
        }
    }

    public static void createAndJoin(int i) {
        new EmbeddedChannel(
                new DummyChannelId("user" + i),
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new LeaveHandler()
        ).writeInbound(
                Quick.toByteBuf("/login user" + i + " pass"),
                Quick.toByteBuf("/join alpha")
        );
    }
}
