package org.example.handlers;

import io.netty.channel.embedded.EmbeddedChannel;
import org.example.Quick;
import org.example.services.RequestDecoder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ListHandlerTest {

    @Test
    void simpleListTest() {
        //given
        EmbeddedChannel channel = new EmbeddedChannel();

        channel.pipeline().addFirst(new RequestDecoder());
        channel.pipeline().addLast(new ChatHandler());
        channel.pipeline().addLast(new LoginHandler());
        channel.pipeline().addLast(new ListHandler());

        //when
        channel.writeInbound(Quick.toByteBuf("/login username1 pass"));
        channel.writeInbound(Quick.toByteBuf("/list"));

        //then
        Assertions.assertEquals("you are logged in as **username1**.", Quick.toText(channel));
        Assertions.assertEquals("alpha\nbeta\nomega\n", Quick.toText(channel));
        Assertions.assertNull(channel.readOutbound());
    }

}