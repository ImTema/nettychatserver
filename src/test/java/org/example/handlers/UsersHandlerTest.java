package org.example.handlers;

import io.netty.channel.embedded.EmbeddedChannel;
import org.example.Quick;
import org.example.services.RequestDecoder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UsersHandlerTest {

    @Test
    void simpleUsersTest() {
        //given
        for (int i = 0; i < 3; i++) {
            Quick.createAndJoin(i);
        }

        EmbeddedChannel channel = new EmbeddedChannel(
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new UsersHandler(),
                new LeaveHandler()
        );


        //when
        channel.writeInbound(Quick.toByteBuf("/login username1 pass"));
        channel.writeInbound(Quick.toByteBuf("/join alpha"));
        channel.writeInbound(Quick.toByteBuf("/users"));

        //then
        Quick.toText(channel);//"Welcome to <laptop name> server!"
        Quick.toText(channel);//"It is <date>> now."
        Assertions.assertEquals("you are logged in as **username1**.", Quick.toText(channel));
        Assertions.assertEquals("Welcome to channel **alpha**!", Quick.toText(channel));
        Assertions.assertEquals("user0\nuser1\nuser2\n", Quick.toText(channel));
        Assertions.assertNull(Quick.toText(channel));
    }

}