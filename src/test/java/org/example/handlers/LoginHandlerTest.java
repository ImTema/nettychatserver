package org.example.handlers;

import io.netty.channel.embedded.EmbeddedChannel;
import org.example.Quick;
import org.example.entity.UserChannel;
import org.example.entity.UserEntity;
import org.example.repository.UserChannelRepository;
import org.example.repository.UserRepository;
import org.example.services.BeanFactory;
import org.example.services.RequestDecoder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LoginHandlerTest {

    @Test
    void login_from_memory() {
        //given
        UserRepository repository = BeanFactory.getBean(UserRepository.class);
        repository.save(new UserEntity("username1", "pass"));

        EmbeddedChannel channel = new EmbeddedChannel();

        channel.pipeline().addFirst(new RequestDecoder());
        channel.pipeline().addLast(new ChatHandler());
        channel.pipeline().addLast(new LoginHandler());
        channel.pipeline().addLast(new JoinHandler());
        channel.pipeline().addLast(new LeaveHandler());

        //when
        channel.writeInbound(Quick.toByteBuf("/login username1 pass"));

        //then
        Assertions.assertEquals("you are logged in as **username1**.", Quick.toText(channel));
        Assertions.assertNull(channel.readOutbound());

        //when
        channel.writeInbound(Quick.toByteBuf("/login username1 pass2"));

        //then
        Assertions.assertEquals("credentials are invalid.", Quick.toText(channel));
    }

    @Test
    void login_from_memory_and_join() {
        //given
        //already saved user
        UserRepository repository = BeanFactory.getBean(UserRepository.class);
        repository.save(new UserEntity("username3", "pass"));

        //already last channel is alpha
        UserChannelRepository userChannelRepository = BeanFactory.getBean(UserChannelRepository.class);
        userChannelRepository.save(new UserChannel("username3", "alpha"));

        EmbeddedChannel channel = new EmbeddedChannel();

        channel.pipeline().addFirst(new RequestDecoder());
        channel.pipeline().addLast(new ChatHandler());
        channel.pipeline().addLast(new LoginHandler());
        channel.pipeline().addLast(new JoinHandler());
        channel.pipeline().addLast(new LeaveHandler());

        //when
        channel.writeInbound(Quick.toByteBuf("/login username3 pass"));

        //then
        Assertions.assertEquals("you are logged in as **username3**.", Quick.toText(channel));
        Assertions.assertEquals("Welcome to channel **alpha**!", Quick.toText(channel));
        Assertions.assertNull(channel.readOutbound());
    }

}
