package org.example.handlers;

import io.netty.channel.embedded.EmbeddedChannel;
import org.example.DummyChannelId;
import org.example.Quick;
import org.example.entity.MessageEntity;
import org.example.entity.UserEntity;
import org.example.models.UserAdapter;
import org.example.repository.MessageRepository;
import org.example.repository.UserRepository;
import org.example.services.BeanFactory;
import org.example.services.RequestDecoder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class JoinHandlerTest {

    @Test
    void join_from_one_device_leads_joining_from_another() {

        //given
        UserRepository repository = BeanFactory.getBean(UserRepository.class);
        repository.save(new UserEntity("username1", "pass"));


        EmbeddedChannel iphone = new EmbeddedChannel(
                new DummyChannelId("iphone"),
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new LeaveHandler()
        );
        EmbeddedChannel pc = new EmbeddedChannel(
                new DummyChannelId("pc"),
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new LeaveHandler()
        );

        //when
        iphone.writeInbound(Quick.toByteBuf("/login username1 pass"));
        pc.writeInbound(Quick.toByteBuf("/login username1 pass"));
        iphone.writeInbound(Quick.toByteBuf("/join alpha"));

        //then
        Assertions.assertEquals("alpha", new UserAdapter(iphone).getChannelName());
        Assertions.assertEquals("alpha", new UserAdapter(pc).getChannelName());
    }

    @Test
    void join_and_show_last_10_messages() {

        //given
        MessageRepository repository = BeanFactory.getBean(MessageRepository.class);
        repository.save(new MessageEntity("alpha", "somewhatUser", "1"));//11
        repository.save(new MessageEntity("alpha", "somewhatUser2", "2"));//10
        repository.save(new MessageEntity("alpha", "somewhatUser", "3"));//9
        repository.save(new MessageEntity("alpha", "somewhatUser2", "4"));//8
        repository.save(new MessageEntity("alpha", "somewhatUser", "5"));//7
        repository.save(new MessageEntity("alpha", "somewhatUser", "6"));//6
        repository.save(new MessageEntity("alpha", "somewhatUser", "7"));//5
        repository.save(new MessageEntity("alpha", "somewhatUser", "8"));//4
        repository.save(new MessageEntity("alpha", "somewhatUser", "9"));//3
        repository.save(new MessageEntity("alpha", "somewhatUser", "10"));//2
        repository.save(new MessageEntity("alpha", "somewhatUser", "11"));//1


        EmbeddedChannel channel = new EmbeddedChannel(
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new LeaveHandler()
        );


        //when
        channel.writeInbound(Quick.toByteBuf("/login username1 pass"));
        channel.writeInbound(Quick.toByteBuf("/join alpha"));

        //then
        Quick.toText(channel);//"Welcome to <laptop name> server!"
        Quick.toText(channel);//"It is<date>> now."
        Assertions.assertEquals("you are logged in as **username1**.", Quick.toText(channel));
        Assertions.assertEquals("Welcome to channel **alpha**!", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser2] 2", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser] 3", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser2] 4", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser] 5", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser] 6", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser] 7", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser] 8", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser] 9", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser] 10", Quick.toText(channel));
        Assertions.assertEquals("[somewhatUser] 11", Quick.toText(channel));
        Assertions.assertNull(Quick.toText(channel));
    }

    @Test
    void dont_join_when_10_participants() {
        //given
        for (int i = 0; i < 10; i++) {
            Quick.createAndJoin(i);
        }

        EmbeddedChannel channel = new EmbeddedChannel(
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new LeaveHandler()
        );


        //when
        channel.writeInbound(Quick.toByteBuf("/login username1 pass"));
        channel.writeInbound(Quick.toByteBuf("/join alpha"));

        //then
        Quick.toText(channel);//"Welcome to <laptop name> server!"
        Quick.toText(channel);//"It is <date>> now."
        Assertions.assertEquals("you are logged in as **username1**.", Quick.toText(channel));
        Assertions.assertEquals("join failed: limit of 10 exceeded.", Quick.toText(channel));
        Assertions.assertNull(Quick.toText(channel));
    }

    @Test
    void join_even_10_but_same_login() {
        //given
        for (int i = 0; i < 9; i++) {
            Quick.createAndJoin(i);
        }

        EmbeddedChannel iphone = new EmbeddedChannel(
                new DummyChannelId("iphone"),
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new LeaveHandler()
        );
        EmbeddedChannel pc = new EmbeddedChannel(
                new DummyChannelId("pc"),
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new LeaveHandler()
        );

        //when
        iphone.writeInbound(Quick.toByteBuf("/login username1 pass"));
        pc.writeInbound(Quick.toByteBuf("/login username1 pass"));
        iphone.writeInbound(Quick.toByteBuf("/join alpha"));

        System.out.println(iphone);
        Quick.log(iphone);
        System.out.println();
        System.out.println(pc);
        Quick.log(pc);

        //then
        Assertions.assertEquals("alpha", new UserAdapter(iphone).getChannelName());
        Assertions.assertEquals("alpha", new UserAdapter(pc).getChannelName());
    }

}

