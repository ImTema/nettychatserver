package org.example.handlers;

import io.netty.channel.embedded.EmbeddedChannel;
import org.example.DummyChannelId;
import org.example.Quick;
import org.example.services.RequestDecoder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PlainTextHandlerTest {

    @Test
    void simplePlainTextTest() {

        //given

        EmbeddedChannel user1 = new EmbeddedChannel(
                new DummyChannelId("user1"),
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new LeaveHandler(),
                new PlainTextHandler()
        );
        EmbeddedChannel user2 = new EmbeddedChannel(
                new DummyChannelId("user2"),
                new RequestDecoder(),
                new ChatHandler(),
                new LoginHandler(),
                new JoinHandler(),
                new LeaveHandler(),
                new PlainTextHandler()
        );

        //when
        user1.writeInbound(Quick.toByteBuf("/login user1 pass"));
        user2.writeInbound(Quick.toByteBuf("/login user2 pass"));
        user1.writeInbound(Quick.toByteBuf("/join alpha"));
        user2.writeInbound(Quick.toByteBuf("/join alpha"));
        user1.writeInbound(Quick.toByteBuf("hello! how are you!"));
        user2.writeInbound(Quick.toByteBuf("cool. u?"));
        user1.writeInbound(Quick.toByteBuf("great. bye"));

        //then
        Quick.toText(user1);//welcome
        Quick.toText(user1);// it is <date> now
        Assertions.assertEquals("you are logged in as **user1**.", Quick.toText(user1));
        Assertions.assertEquals("Welcome to channel **alpha**!", Quick.toText(user1));
        Assertions.assertEquals("[you] hello! how are you!", Quick.toText(user1));
        Assertions.assertEquals("[user2] cool. u?", Quick.toText(user1));
        Assertions.assertEquals("[you] great. bye", Quick.toText(user1));

        Quick.toText(user2);//welcome
        Quick.toText(user2);// it is <date> now
        Assertions.assertEquals("you are logged in as **user2**.", Quick.toText(user2));
        Assertions.assertEquals("Welcome to channel **alpha**!", Quick.toText(user2));
        Assertions.assertEquals("[user1] hello! how are you!", Quick.toText(user2));
        Assertions.assertEquals("[you] cool. u?", Quick.toText(user2));
        Assertions.assertEquals("[user1] great. bye", Quick.toText(user2));

    }

}

