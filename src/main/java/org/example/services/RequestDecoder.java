package org.example.services;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import org.example.models.MessageFactory;

import java.nio.charset.Charset;
import java.util.List;

@ChannelHandler.Sharable
public class RequestDecoder extends MessageToMessageDecoder<ByteBuf> {


    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        System.out.println("received: " + msg);
        var string = msg.toString(Charset.defaultCharset());
        if (string.isEmpty()) {
            return;
        }

        var request = MessageFactory.createMessage(string);
        out.add(request);
    }
}