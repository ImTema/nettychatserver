package org.example.services;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.internal.ObjectUtil;
import org.example.models.ResponseMessage;

import java.nio.charset.Charset;

@ChannelHandler.Sharable
public class ResponseEncoder extends MessageToByteEncoder<ResponseMessage> {

    private final Charset charset;

    /**
     * Creates a new instance with the current system character set.
     */
    public ResponseEncoder() {
        this(Charset.defaultCharset());
    }


    /**
     * Creates a new instance with the specified character set.
     */
    public ResponseEncoder(Charset charset) {
        this.charset = ObjectUtil.checkNotNull(charset, "charset");
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, ResponseMessage msg, ByteBuf out) throws Exception {
        if (msg == null) {
            return;
        }

        out.writeCharSequence(msg.getMessage() + "\n", charset);
    }
}