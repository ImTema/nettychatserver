package org.example.services;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.example.models.UserAdapter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ChannelGroupManager {

    private static final ChannelGroup onlineUsers = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    //linked hash map for ordering
    private static final Map<String, ChannelGroup> channels = new LinkedHashMap<>();

    static {
        channels.put("alpha", new DefaultChannelGroup(GlobalEventExecutor.INSTANCE));
        channels.put("beta", new DefaultChannelGroup(GlobalEventExecutor.INSTANCE));
        channels.put("omega", new DefaultChannelGroup(GlobalEventExecutor.INSTANCE));
    }

    public boolean exists(String channelName) {
        return channels.get(channelName) != null;
    }

    public void registerUser(Channel user) {
        onlineUsers.add(user);
    }

    public void addUserToGroup(String channelName, Channel user) {
        validateExistence(channelName);
        channels.get(channelName).add(user);
    }

    public void removeUserFromGroup(String channelName, Channel user) {
        validateExistence(channelName);
        channels.get(channelName).remove(user);
    }

    public List<String> findAllChannelNames(int page, int size) {
        return channels.keySet().stream()
                .skip((long) page * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public ChannelGroup getChannelParticipants(String channelName) {
        validateExistence(channelName);
        return channels.get(channelName);
    }

    public void processAnotherSessions(UserAdapter initiator, Consumer<Channel> block) {
        onlineUsers.stream()
                .filter(it -> {
                    return new UserAdapter(it).getUsername().equals(initiator.getUsername()) && it != initiator.getChannel();
                }).forEach(block);
    }

    public void validateExistence(String channelName) {
        if (!exists(channelName)) {
            throw new RuntimeException("channel name **" + channelName + "** not found");
        }
    }
}
