package org.example.services;

import org.example.handlers.*;
import org.example.repository.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BeanFactory {

    static Map<Class<?>, Object> beans = new ConcurrentHashMap<>();

    static {
        //priority
        beans.put(ChannelGroupManager.class, new ChannelGroupManager());

        beans.put(UserRepository.class, new InMemoryUserRepository());
        beans.put(MessageRepository.class, new InMemoryMessageRepository());
        beans.put(UserChannelRepository.class, new InMemoryUserChannelRepository());

        beans.put(ChatHandler.class, new ChatHandler());
        beans.put(UsersHandler.class, new UsersHandler());
        beans.put(JoinHandler.class, new JoinHandler());
        beans.put(LeaveHandler.class, new LeaveHandler());
        beans.put(ListHandler.class, new ListHandler());
        beans.put(LoginHandler.class, new LoginHandler());
        beans.put(PlainTextHandler.class, new PlainTextHandler());
    }

    private BeanFactory() {
    }

    public static <T> T getBean(Class<T> tClass) {
        return (T) beans.get(tClass);
    }
}
