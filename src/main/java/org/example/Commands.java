package org.example;

public class Commands {

    /**
     * /login <name> <password>
     * If the user doesn’t exist, create profile else login, after login join to last connected
     * channel (use join logic, if client’s limit exceeded, keep connected, but without active
     * channel).
     */
    public static final String LOGIN = "login";
    /**
     * /join <channel>
     * Try to join a channel (max 10 active clients per channel is needed). If client's limit
     * exceeded - send error, otherwise join channel and send last N messages of activity. ●
     */
    public static final String JOIN = "join";
    /**
     * /leave
     * Leave current channel.
     */
    public static final String LEAVE = "leave";
    /**
     * /disconnect
     * Close connection to server.
     */
    public static final String DISCONNECT = "disconnect";
    /**
     * /list
     * Send list of channels.
     */
    public static final String LIST = "list";
    /**
     * /users
     * Send list of unique users in current channel.
     */
    public static final String USERS = "users";

    /**
     * <text message terminated with CR>
     * Sends message to current channel Server must send a new message to all connected to
     * this channel.
     */
    public static final String PLAIN_TEXT = "plain-text";
}
