package org.example.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.example.Commands;

@AllArgsConstructor
@Getter
public class LeaveMessage implements IncomingMessage {

    private final boolean nested;

    @Override
    public String getCommand() {
        return Commands.LEAVE;
    }

}
