package org.example.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.example.Commands;

@Data
@AllArgsConstructor
public class PlainTextMessage implements IncomingMessage {

    private String text;

    @Override
    public String getCommand() {
        return Commands.PLAIN_TEXT;
    }
}
