package org.example.models;

public interface IncomingMessage {
    String getCommand();
}
