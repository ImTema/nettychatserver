package org.example.models;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.AttributeKey;

public class UserAdapter {

    private final Channel channel;
    private final AttributeKey<String> usernameAttr = AttributeKey.valueOf("username");
    private final AttributeKey<String> channelNameAttr = AttributeKey.valueOf("channelName");
    public UserAdapter(ChannelHandlerContext ctx) {
        this.channel = ctx.channel();
    }
    public UserAdapter(Channel channel) {
        this.channel = channel;
    }

    public String getUsername() {
        return channel.attr(usernameAttr).get();
    }

    public void setUsername(String username) {
        channel.attr(usernameAttr).set(username);
    }

    public boolean isLoggedIn() {
        return getUsername() != null;
    }

    public String getChannelName() {
        return channel.attr(channelNameAttr).get();
    }

    public void setChannelName(String channelName) {
        channel.attr(channelNameAttr).set(channelName);
    }

    public Channel getChannel() {
        return channel;
    }
}
