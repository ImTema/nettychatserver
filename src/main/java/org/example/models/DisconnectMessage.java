package org.example.models;

import org.example.Commands;

public class DisconnectMessage implements IncomingMessage {
    @Override
    public String getCommand() {
        return Commands.DISCONNECT;
    }

}
