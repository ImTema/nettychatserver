package org.example.models;

import org.example.Commands;

public class UsersMessage implements IncomingMessage {
    @Override
    public String getCommand() {
        return Commands.USERS;
    }
}
