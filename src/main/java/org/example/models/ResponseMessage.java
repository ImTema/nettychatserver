package org.example.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class ResponseMessage {
    private String message;
    private boolean success;
    private Map<String, Object> details;

    public ResponseMessage(String message) {
        this.message = message;
    }

    public static ResponseMessage ok(String message) {
        return new ResponseMessage(message, true, null);
    }

    public static ResponseMessage echo(String author, String message) {
        return new ResponseMessage("[" + author + "] " + message, true, null);
    }

    public static ResponseMessage error(String message) {
        return new ResponseMessage(message, false, null);
    }

    public static ResponseMessage unauthorized() {
        return new ResponseMessage("not authorized: use /login <name> <password>", false, null);
    }


}