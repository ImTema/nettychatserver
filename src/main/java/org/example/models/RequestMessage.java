package org.example.models;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class RequestMessage implements IncomingMessage {
    private String command;
    @Getter(AccessLevel.PROTECTED)
    private List<String> args = new ArrayList<>();

    public String getArg(int index) {
        if (index < args.size()) {
            return args.get(index);
        }
        return null;
    }


}