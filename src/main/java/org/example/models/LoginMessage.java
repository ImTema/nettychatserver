package org.example.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.example.Commands;

@AllArgsConstructor
@Data
public class LoginMessage implements IncomingMessage {

    private String username;
    private String password;

    @Override
    public String getCommand() {
        return Commands.LOGIN;
    }

}
