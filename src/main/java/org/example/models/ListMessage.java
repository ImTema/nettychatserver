package org.example.models;

import org.example.Commands;

public class ListMessage implements IncomingMessage {
    @Override
    public String getCommand() {
        return Commands.LIST;
    }

}
