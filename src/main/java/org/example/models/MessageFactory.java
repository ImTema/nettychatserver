package org.example.models;

import org.example.Commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessageFactory {

    private final static String DEFAULT_COMMAND = Commands.PLAIN_TEXT;

    public static IncomingMessage createMessage(String string) {
        String command = DEFAULT_COMMAND;
        List<String> args = new ArrayList<>();
        if (string.charAt(0) == '/') {
            var chunks = string.split(" ");
            //to remove '/'
            command = chunks[0].substring(1);
            args.addAll(Arrays.asList(chunks).subList(1, chunks.length));
        } else {
            args.add(string);
        }

        //compile message
        var unknown = new RequestMessage();
        unknown.setCommand(command);
        unknown.setArgs(args);
        return unknown;

    }
}
