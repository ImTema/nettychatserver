package org.example.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.example.Commands;


@AllArgsConstructor
@Getter
public class JoinMessage implements IncomingMessage {
    private final String channelName;
    private final boolean nested;

    @Override
    public String getCommand() {
        return Commands.JOIN;
    }

}
