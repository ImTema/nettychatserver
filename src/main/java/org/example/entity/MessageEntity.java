package org.example.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class MessageEntity {

    private UUID id;
    private String channelName;
    private String author;
    private String text;
    private LocalDateTime createdAt;

    public MessageEntity(String channelName, String author, String text) {
        this.id = UUID.randomUUID();
        this.channelName = channelName;
        this.author = author;
        this.text = text;
        this.createdAt = LocalDateTime.now();
    }
}
