package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class UserChannel {
    private String username;
    private String channelName;

    @Data
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class PK implements Serializable {
        private String username;
        private String channelName;
    }
}
