package org.example.repository;

import org.example.entity.MessageEntity;

import java.util.List;

public interface MessageRepository {

    MessageEntity save(MessageEntity message);

    List<MessageEntity> findAll(String channelName, int page, int size, boolean asc);
}
