package org.example.repository;

import org.example.entity.UserChannel;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryUserChannelRepository implements UserChannelRepository {

    private final Map<String, Set<UserChannel.PK>> channelUserNames = new ConcurrentHashMap<>();
    private final Map<String, UserChannel.PK> usernameChannel = new ConcurrentHashMap<>();

    private final Map<UserChannel.PK, UserChannel> store = new ConcurrentHashMap<>();

    @Override
    public Optional<UserChannel> findByUsername(String username) {
        if (usernameChannel.get(username) == null) {
            return Optional.empty();
        }
        return Optional.of(store.get(usernameChannel.get(username)));
    }

    @Override
    public List<UserChannel> findByChannel(String channelName, int page, int size) {
        return channelUserNames.getOrDefault(channelName, Set.of())
                .stream()
                .sorted(Comparator.comparing(UserChannel.PK::getUsername))
                .skip(page)
                .limit(size)
                .map(store::get)
                .collect(Collectors.toList());
    }

    @Override
    public UserChannel save(UserChannel userChannel) {
        UserChannel.PK pk = new UserChannel.PK(userChannel.getUsername(), userChannel.getChannelName());
        channelUserNames.computeIfAbsent(userChannel.getChannelName(), k -> new HashSet<>());
        channelUserNames.get(userChannel.getChannelName()).add(pk);
        usernameChannel.put(userChannel.getUsername(), pk);
        store.put(pk, userChannel);
        return userChannel;
    }

    @Override
    public void delete(UserChannel userChannel) {
        UserChannel.PK pk = new UserChannel.PK(userChannel.getUsername(), userChannel.getChannelName());
        channelUserNames.get(userChannel.getChannelName()).remove(pk);
        usernameChannel.remove(userChannel.getUsername());
        store.remove(pk);
    }
}
