package org.example.repository;

import org.example.entity.UserChannel;

import java.util.List;
import java.util.Optional;

public interface UserChannelRepository {

    Optional<UserChannel> findByUsername(String username);

    List<UserChannel> findByChannel(String channelName, int page, int size);

    UserChannel save(UserChannel userChannel);

    void delete(UserChannel userChannel);
}
