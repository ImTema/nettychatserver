package org.example.repository;

import org.example.entity.UserEntity;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryUserRepository implements UserRepository {
    private final ConcurrentHashMap<String, UserEntity> store = new ConcurrentHashMap<>();

    @Override
    public Optional<UserEntity> findByUsername(String username) {
        return Optional.ofNullable(store.get(username));
    }

    @Override
    public UserEntity save(UserEntity user) {
        return store.put(user.getUsername(), user);
    }
}
