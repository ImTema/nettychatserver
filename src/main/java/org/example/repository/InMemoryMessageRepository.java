package org.example.repository;

import org.example.entity.MessageEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryMessageRepository implements MessageRepository {

    ConcurrentHashMap<UUID, MessageEntity> store = new ConcurrentHashMap<>();
    ConcurrentHashMap<String, List<MessageEntity>> indexByChannelName = new ConcurrentHashMap<>();

    @Override
    public MessageEntity save(MessageEntity message) {
        store.put(message.getId(), message);
        indexByChannelName.computeIfAbsent(message.getChannelName(), k -> new ArrayList<>());
        indexByChannelName.get(message.getChannelName()).add(message);
        return message;
    }

    @Override
    public List<MessageEntity> findAll(String channelName, int page, int size, boolean asc) {
        var comparator = Comparator.comparing(MessageEntity::getCreatedAt);
        if (!asc) {
            comparator = comparator.reversed();
        }
        indexByChannelName.computeIfAbsent(channelName, k -> new ArrayList<>());
        return indexByChannelName.get(channelName).stream()
                .sorted(comparator)
                .skip((long) size * page)
                .limit(size)
                .collect(Collectors.toList());
    }
}
