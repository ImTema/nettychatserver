package org.example.repository;

import org.example.entity.UserEntity;

import java.util.Optional;

public interface UserRepository {

    Optional<UserEntity> findByUsername(String username);

    UserEntity save(UserEntity user);
}
