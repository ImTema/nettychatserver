package org.example.handlers;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.example.entity.MessageEntity;
import org.example.entity.UserChannel;
import org.example.models.JoinMessage;
import org.example.models.LeaveMessage;
import org.example.models.ResponseMessage;
import org.example.models.UserAdapter;
import org.example.repository.MessageRepository;
import org.example.repository.UserChannelRepository;
import org.example.services.BeanFactory;
import org.example.services.ChannelGroupManager;

import java.util.Comparator;

@ChannelHandler.Sharable
public class JoinHandler extends SimpleChannelInboundHandler<JoinMessage> {

    private final ChannelGroupManager channelGroupManager = BeanFactory.getBean(ChannelGroupManager.class);
    private final UserChannelRepository userChannelRepository = BeanFactory.getBean(UserChannelRepository.class);
    private final MessageRepository messageRepository = BeanFactory.getBean(MessageRepository.class);

    private final Integer MAXIMUM_PARTICIPANTS = 10;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, JoinMessage msg) {
        var userCtx = new UserAdapter(ctx);

        var response = joinChannel(ctx, userCtx, msg);
        ctx.writeAndFlush(response);

        if (response.isSuccess()) {
            //display last 10 messages
            messageRepository.findAll(msg.getChannelName(), 0, 10, false)
                    .stream()
                    .sorted(Comparator.comparing(MessageEntity::getCreatedAt))
                    .forEach(it -> {
                        String author;
                        if (it.getAuthor().equals(userCtx.getUsername())) {
                            author = "you";
                        } else {
                            author = it.getAuthor();
                        }
                        ctx.writeAndFlush(ResponseMessage.echo(author, it.getText()));
                    });
        }
    }

    private ResponseMessage joinChannel(ChannelHandlerContext ctx, UserAdapter userCtx, JoinMessage msg) {
        if (!userCtx.isLoggedIn()) {
            return ResponseMessage.unauthorized();
        }
        var channelName = msg.getChannelName();
        if (channelName == null) {
            return ResponseMessage.error("not enough arguments: /join <channelName>");
        }
        //maximum ACTIVE DIFFERENT users
        if (channelGroupManager.getChannelParticipants(channelName).stream()
                .map(it -> new UserAdapter(it).getUsername())
                .distinct()
                //join from different devices
                .filter(it -> !it.equals(userCtx.getUsername()))
                .count() >= MAXIMUM_PARTICIPANTS) {
            return ResponseMessage.error("join failed: limit of " + MAXIMUM_PARTICIPANTS + " exceeded.");
        }

        //leave current channel
        ctx.pipeline().get(LeaveHandler.class).channelRead0(ctx, new LeaveMessage(msg.isNested()));

        //join new channel
        channelGroupManager.addUserToGroup(channelName, userCtx.getChannel());

        //if nested we should not repeat same logic twice
        if (!msg.isNested()) {
            userChannelRepository.save(new UserChannel(userCtx.getUsername(), channelName));
            //check if logged in from another devices / sessions
            channelGroupManager.processAnotherSessions(userCtx, it -> {
                it.pipeline().fireChannelRead(new JoinMessage(channelName, true));
            });
        }

        userCtx.setChannelName(channelName);
        return ResponseMessage.ok("Welcome to channel **" + channelName + "**!");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.writeAndFlush(ResponseMessage.error(cause.getMessage()));
    }
}
