package org.example.handlers;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.example.entity.UserEntity;
import org.example.models.JoinMessage;
import org.example.models.LoginMessage;
import org.example.models.ResponseMessage;
import org.example.models.UserAdapter;
import org.example.repository.UserChannelRepository;
import org.example.repository.UserRepository;
import org.example.services.BeanFactory;


@ChannelHandler.Sharable
public class LoginHandler extends SimpleChannelInboundHandler<LoginMessage> {
    private final UserRepository userRepository = BeanFactory.getBean(UserRepository.class);
    private final UserChannelRepository userChannelRepository = BeanFactory.getBean(UserChannelRepository.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginMessage msg) throws Exception {
        var userCtx = new UserAdapter(ctx);

        var response = login(userCtx, msg);

        ctx.writeAndFlush(response);

        if (response.isSuccess()) {
            var lastChannelName = userChannelRepository.findByUsername(userCtx.getUsername());
            lastChannelName.ifPresent(it -> ctx.pipeline().get(JoinHandler.class).channelRead0(ctx, new JoinMessage(it.getChannelName(), false)));
        }
    }

    private ResponseMessage login(UserAdapter userCtx, LoginMessage msg) {
        var username = msg.getUsername();
        var password = msg.getPassword();
        if (username == null || password == null) {
            return ResponseMessage.error("not enough arguments: /login <name> <password>");
        }
        var user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            userRepository.save(new UserEntity(username, password));
            userCtx.setUsername(username);
        } else if (user.get().getPassword().equals(password)) {
            userCtx.setUsername(username);
        } else {
            userCtx.setUsername(null);
        }
        if (userCtx.isLoggedIn()) {
            return ResponseMessage.ok("you are logged in as **" + userCtx.getUsername() + "**.");
        }
        return ResponseMessage.error("credentials are invalid.");
    }
}
