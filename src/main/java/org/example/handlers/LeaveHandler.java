package org.example.handlers;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.internal.StringUtil;
import org.example.entity.UserChannel;
import org.example.models.LeaveMessage;
import org.example.models.ResponseMessage;
import org.example.models.UserAdapter;
import org.example.repository.UserChannelRepository;
import org.example.services.BeanFactory;
import org.example.services.ChannelGroupManager;

@ChannelHandler.Sharable
public class LeaveHandler extends SimpleChannelInboundHandler<LeaveMessage> {

    private final ChannelGroupManager channelGroupManager = BeanFactory.getBean(ChannelGroupManager.class);
    private final UserChannelRepository userChannelRepository = BeanFactory.getBean(UserChannelRepository.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LeaveMessage msg) {
        var userCtx = new UserAdapter(ctx);

        var response = leaveChannel(userCtx, msg);

        if (StringUtil.isNullOrEmpty(response.getMessage())) {
            return;
        }
        ctx.writeAndFlush(response);

    }

    private ResponseMessage leaveChannel(UserAdapter userCtx, LeaveMessage msg) {
        var channelName = userCtx.getChannelName();
        if (channelName == null) {
            return ResponseMessage.ok("");
        }
        channelGroupManager.removeUserFromGroup(channelName, userCtx.getChannel());
        userCtx.setChannelName(null);
        if (!msg.isNested()) {
            userChannelRepository.delete(new UserChannel(userCtx.getUsername(), channelName));

            //check if logged in from another devices / sessions
            channelGroupManager.processAnotherSessions(userCtx, it -> {
                it.pipeline().fireChannelRead(new LeaveMessage(true));
            });
        }

        return ResponseMessage.ok("you left channel **" + channelName + "**!");
    }
}
