package org.example.handlers;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.example.entity.MessageEntity;
import org.example.models.PlainTextMessage;
import org.example.models.ResponseMessage;
import org.example.models.UserAdapter;
import org.example.repository.MessageRepository;
import org.example.services.BeanFactory;
import org.example.services.ChannelGroupManager;


@ChannelHandler.Sharable
public class PlainTextHandler extends SimpleChannelInboundHandler<PlainTextMessage> {

    private final ChannelGroupManager channelGroupManager = BeanFactory.getBean(ChannelGroupManager.class);
    private final MessageRepository messageRepository = BeanFactory.getBean(MessageRepository.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, PlainTextMessage msg) throws Exception {
        var userCtx = new UserAdapter(ctx);

        var response = plainText(userCtx, msg);
        ctx.writeAndFlush(response);
    }

    private ResponseMessage plainText(UserAdapter userCtx, PlainTextMessage msg) {
        if (!userCtx.isLoggedIn()) {
            return ResponseMessage.ok("use /login <name> <password>");
        }
        var channelName = userCtx.getChannelName();
        if (channelName == null) {
            return ResponseMessage.ok("join channel first /join <channelName>");
        }
        messageRepository.save(new MessageEntity(channelName, userCtx.getUsername(), msg.getText()));
        ResponseMessage responseMessage = null;
        for (Channel participant : channelGroupManager.getChannelParticipants(channelName)) {
            if (participant != userCtx.getChannel()) {
                UserAdapter adapter = new UserAdapter(participant);
                String author;
                if (adapter.getUsername().equals(userCtx.getUsername())) {
                    author = "you";
                } else {
                    author = userCtx.getUsername();
                }
                participant.writeAndFlush(ResponseMessage.echo(author, msg.getText()));
            } else {
                responseMessage = ResponseMessage.echo("you", msg.getText());
            }
        }
        return responseMessage;
    }
}
