package org.example.handlers;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.example.models.*;
import org.example.services.BeanFactory;
import org.example.services.ChannelGroupManager;

import java.net.InetAddress;
import java.util.Date;

import static org.example.Commands.*;

/**
 * Handles a server-side channel.
 */
@Sharable
public class ChatHandler extends SimpleChannelInboundHandler<RequestMessage> {

    private final ChannelGroupManager channelGroupManager = BeanFactory.getBean(ChannelGroupManager.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.write(ResponseMessage.ok("Welcome to " + InetAddress.getLocalHost().getHostName() + " server!"));
        ctx.write(ResponseMessage.ok("It is " + new Date() + " now."));
        channelGroupManager.registerUser(ctx.channel());
        ctx.flush();
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, RequestMessage request) throws Exception {
        System.out.println("command: " + request);
        ResponseMessage response = null;
        boolean close = false;
        var command = request.getCommand();

        switch (command) {
            case LOGIN -> ctx.fireChannelRead(new LoginMessage(request.getArg(0), request.getArg(1)));
            case JOIN -> ctx.fireChannelRead(new JoinMessage(request.getArg(0), false));
            case LEAVE -> ctx.fireChannelRead(new LeaveMessage(false));
            case DISCONNECT -> {
                response = ResponseMessage.ok("Disconnecting...");
                close = true;
            }
            case LIST -> ctx.pipeline().get(ListHandler.class)
                    .channelRead0(ctx, new ListMessage());
            case USERS -> ctx.pipeline().get(UsersHandler.class)
                    .channelRead0(ctx, new UsersMessage());
            case PLAIN_TEXT -> ctx.pipeline().get(PlainTextHandler.class)
                    .channelRead0(ctx, new PlainTextMessage(request.getArg(0)));
            default -> response = ResponseMessage.error("unknown command: /" + request.getCommand());
        }

        ChannelFuture future;
        if (response != null) {
            future = ctx.writeAndFlush(response);
        } else {
            future = ctx.newSucceededFuture();
        }
        if (close) {
            future.addListener(ChannelFutureListener.CLOSE);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.write(new ResponseMessage("bruh" + cause));
        ctx.close();
    }
}