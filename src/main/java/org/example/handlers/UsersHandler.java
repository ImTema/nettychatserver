package org.example.handlers;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.example.models.ResponseMessage;
import org.example.models.UserAdapter;
import org.example.models.UsersMessage;
import org.example.services.BeanFactory;
import org.example.services.ChannelGroupManager;


@ChannelHandler.Sharable
public class UsersHandler extends SimpleChannelInboundHandler<UsersMessage> {

    private final ChannelGroupManager channelGroupManager = BeanFactory.getBean(ChannelGroupManager.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, UsersMessage msg) throws Exception {
        var userCtx = new UserAdapter(ctx);

        var response = users(userCtx, msg);

        ctx.writeAndFlush(response);
    }

    private ResponseMessage users(UserAdapter userCtx, UsersMessage msg) {
        if (!userCtx.isLoggedIn()) {
            return ResponseMessage.unauthorized();
        }

        var channelName = userCtx.getChannelName();
        if (channelName == null) {
            return ResponseMessage.ok("join some channel first: /join <channelName>");
        }

        //todo add pagination
        var offset = 0;
        var size = 10;
        var sb = new StringBuilder();
        channelGroupManager.getChannelParticipants(channelName)
                .stream()
                .skip(offset)
                .limit(size)
                .forEach(it -> {
                    if (it != userCtx.getChannel()) {
                        sb.append(new UserAdapter(it).getUsername()).append("\n");
                    }
                });

        return ResponseMessage.ok(sb.isEmpty() ? "empty" : sb.toString());
    }
}
