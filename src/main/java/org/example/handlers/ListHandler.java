package org.example.handlers;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.example.models.ListMessage;
import org.example.models.ResponseMessage;
import org.example.models.UserAdapter;
import org.example.services.BeanFactory;
import org.example.services.ChannelGroupManager;


@ChannelHandler.Sharable
public class ListHandler extends SimpleChannelInboundHandler<ListMessage> {

    private final ChannelGroupManager channelGroupManager = BeanFactory.getBean(ChannelGroupManager.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ListMessage msg) throws Exception {
        var userCtx = new UserAdapter(ctx);

        var response = listChannel(userCtx, msg);

        ctx.writeAndFlush(response);
    }

    private ResponseMessage listChannel(UserAdapter userCtx, ListMessage msg) {
        if (!userCtx.isLoggedIn()) {
            return ResponseMessage.unauthorized();
        }
        var sb = new StringBuilder();
        //todo pagination feature
        var page = 0;
        var size = 10;
        channelGroupManager.findAllChannelNames(page, size)
                .forEach(it -> sb.append(it).append("\n"));

        return ResponseMessage.ok(sb.isEmpty() ? "empty" : sb.toString());
    }
}
