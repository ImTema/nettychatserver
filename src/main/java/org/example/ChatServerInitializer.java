package org.example;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.ssl.SslContext;
import org.example.handlers.*;
import org.example.services.BeanFactory;
import org.example.services.RequestDecoder;
import org.example.services.ResponseEncoder;

/**
 * Creates a newly configured {@link ChannelPipeline} for a new channel.
 */
public class ChatServerInitializer extends ChannelInitializer<SocketChannel> {

    private static final RequestDecoder DECODER = new RequestDecoder();
    private static final ResponseEncoder ENCODER = new ResponseEncoder();

    private final SslContext sslCtx;

    public ChatServerInitializer(SslContext sslCtx) {
        this.sslCtx = sslCtx;
    }

    @Override
    public void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        if (sslCtx != null) {
            pipeline.addLast(sslCtx.newHandler(ch.alloc()));
        }

        // Add the text line codec combination first,
        pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        // the encoder and decoder are static as these are sharable
        pipeline.addLast(DECODER);
        pipeline.addLast(ENCODER);

        // and then business logic.
        pipeline.addLast(BeanFactory.getBean(ChatHandler.class));
        pipeline.addLast(BeanFactory.getBean(LoginHandler.class));
        pipeline.addLast(BeanFactory.getBean(JoinHandler.class));
        pipeline.addLast(BeanFactory.getBean(LeaveHandler.class));
        pipeline.addLast(BeanFactory.getBean(ListHandler.class));
        pipeline.addLast(BeanFactory.getBean(UsersHandler.class));
        pipeline.addLast(BeanFactory.getBean(PlainTextHandler.class));
        pipeline.addLast(BeanFactory.getBean(UsersHandler.class));
    }
}